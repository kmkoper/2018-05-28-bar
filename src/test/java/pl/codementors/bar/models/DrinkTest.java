package pl.codementors.bar.models;

import org.junit.Test;
import org.junit.Assert;

public class DrinkTest {
    @Test
    public void setNameTest(){
        Drink drink = new Drink();
        drink.setName("Mohito");
        Assert.assertEquals("Mohito", drink.getName());
    }
    @Test
    public void toStringWhenNameSet(){
        Drink drink = new Drink("Mohito");
                Assert.assertEquals("Drink: Mohito", drink.toString());
    }
}
