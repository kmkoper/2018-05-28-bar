package pl.codementors.bar.models;

import org.junit.Assert;
import org.junit.Test;

public class BarTest {
    Bar bar;
    Drink d1 = new Drink("Mohito");
    Drink d2 = new Drink("Sunrise");

    @Test(timeout = 100)
    public void barIsWorking() throws InterruptedException {
        bar.put(d1);
        Assert.assertEquals("Bar is not working", d1, bar.take());
    }
}
