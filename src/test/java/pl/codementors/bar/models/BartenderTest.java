package pl.codementors.bar.models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import java.util.Scanner;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
public class BartenderTest {
    Bar bar;
    Drink drink;
    Bartender bartender;
    @Before
    public void prepare() {
        bar = new Bar();
        bartender = new Bartender(bar);
        drink = new Drink("Mohito");
    }

    @Test
    public void BartenderIsWorking() throws InterruptedException{
        Scanner mockScanner = mock(Scanner.class);
        bartender.run();
        when(mockScanner.nextLine()).thenReturn("Mohito");

        Assert.assertNotNull(bar.take());
    }
}
