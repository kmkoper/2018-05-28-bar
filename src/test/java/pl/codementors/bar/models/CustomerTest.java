package pl.codementors.bar.models;

import org.junit.Assert;
import org.junit.Test;

public class CustomerTest {
    Bar bar;
    Customer customer;

    @Test
    public void CustomerWhenNameIsProvided() {
        customer = new Customer("Darek", bar);
        bar = new Bar();
        Assert.assertEquals("Darek", customer.getName());
    }
}
