package pl.codementors.bar.models;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Customer is waiting by the bar at bar.take() method (wait). When Bartender put() the drink
 * customer drinks it.
 * Customer drink booze for as much seconds as drink name has letters.
 */
public class Customer implements Runnable {
    private String name = null;
    private static final Logger log = Logger.getLogger(Customer.class.getCanonicalName());
    private Bar bar;
    private boolean running = true;

    /**
     * @param name name of customer
     * @param bar  at which bar customer is.
     */

    public Customer(String name, Bar bar) {
        this.name = name;
        this.bar = bar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void stop() {
        running = false;
    }


    /**
     * Customer is waiting by the bar at bar.take() method (wait). When Bartender put() the drink
     * customer drinks it.
     * Customer drink booze for as much seconds as drink name has letters.
     */
    @Override
    public void run() {
        while (running) {
            try {
                synchronized (this) {
                    Drink drink = bar.take();
                    System.out.println(name + " " + drink);
                    int howLong = drink.getName().length() * 1000 + 1; //+1 to keep thread alive when empty line is given.
                    wait(howLong);
                    System.out.println(name + ": Poproszę następnego drinka!");
                }
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }

        }
    }
}
