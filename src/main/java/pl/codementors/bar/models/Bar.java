package pl.codementors.bar.models;

/**
 * Bar Class. Customer and Bartender meets at the bar.
 */
public class Bar {
    private Drink drink;
    private Drink poisonousDrink;

    /**
     * Taking drink from a bar. Take method is waking when there is drink to take.
     *
     * @return object Drink.
     * @throws InterruptedException
     */

    public synchronized Drink take() throws InterruptedException {
        while (drink == null) {
            this.wait();
        }
        Drink drinkToDrink = drink;
        drink = null;
        return drinkToDrink;
    }

    /**
     * This method is putting drink on a bar.
     *
     * @param drink to put
     */

    public synchronized void put(Drink drink) {
        this.drink = drink;
        notifyAll();
    }

    /**
     * Methods for closing the Bar.
     *
     * @param poisonousDrink
     */
    public synchronized void putPoisonousDrink(Drink poisonousDrink) {
        this.poisonousDrink = poisonousDrink;
        notifyAll();
    }

    public synchronized Drink takePoisonousDrink() throws InterruptedException {
        while (poisonousDrink == null) {
            this.wait();
        }
        return poisonousDrink;
    }
}
