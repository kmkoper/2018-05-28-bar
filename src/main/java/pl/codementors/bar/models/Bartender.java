package pl.codementors.bar.models;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bartender class for preparing drinks.
 * Bartender call Put mehtod from Bar class when he get the drink name
 */
public class Bartender implements Runnable {
    private static final Logger log = Logger.getLogger(Bartender.class.getCanonicalName());
    private Bar bar;
    private Scanner input = new Scanner(System.in);
    private Drink drink = new Drink();
    private Drink poisonousDrink = new Drink();
    private boolean running = true;


    /**
     * @param bar Bartender should know where he works.
     */
    public Bartender(Bar bar) {
        this.bar = bar;
    }

    /**
     * Bartender ask you to say what drink do you want
     * Preparation time is 2 sec.
     * To stop the app type exit.
     */
    @Override
    public void run() {
        while (running) {
            String drinkName = drinkOrder();
            String exit = "exit";

            if (drinkName.equals(exit)) {
                puttingPoisonousDrink();
                running = false;
            } else if (!drinkName.equals(exit)) {
                makingDrink(drinkName);
            }

        }
    }

    /**
     * Bartender asks for drink name which you want
     *
     * @return drink name
     */
    private String drinkOrder() {
        System.out.println("### Jakiego chcesz drinka? ###");
        String drinkName = input.nextLine();
        return drinkName;
    }

    /**
     * Bartender makes normal drink
     *
     * @param drinkName Name of drink which bartender will make.
     */
    private void makingDrink(String drinkName) {
        drink.setName(drinkName);
        System.out.println("### Przygotowuję drinka: " + drinkName + " ###");
        try {
            synchronized (this) {
                wait(2000);
                bar.put(drink);
                wait(100); //waiter to better look in console. Now put is before question about new drink.
            }
        } catch (InterruptedException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    /**
     * Method for pass signal about killing app.
     */
    private void puttingPoisonousDrink() {
        bar.putPoisonousDrink(poisonousDrink); //waking up thread which is waiting on method takepoisonousdrink().
    }
}


