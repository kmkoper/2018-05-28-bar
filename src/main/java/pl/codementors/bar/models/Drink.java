package pl.codementors.bar.models;

/**
 * Drink Class
 */
public class Drink {
    private String name = null;

    public Drink() {
    }

    public Drink(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "pije: " + name;
    }
}
