package pl.codementors.bar;

import pl.codementors.bar.models.Bar;
import pl.codementors.bar.models.Bartender;
import pl.codementors.bar.models.Customer;
import pl.codementors.bar.models.Drink;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bar Main Class
 *
 * @author Krzysztof Koper
 */

public class BarMain {
    private static final Logger log = Logger.getLogger(BarMain.class.getCanonicalName());

    /**
     * app main method
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Welcome to the Bar!");
        atTheBar();

    }

    private static void atTheBar() {

        Bar bar = new Bar();

        Bartender bartender = new Bartender(bar);
        Customer customer1 = new Customer("Jan", bar);
        Customer customer2 = new Customer("Kamil", bar);
        Customer customer3 = new Customer("Krzysztof", bar);
        Customer customer4 = new Customer("Michał", bar);
        Customer customer5 = new Customer("Łukasz", bar);

        Thread threadBartender = new Thread(bartender);
        Thread threadCustomer1 = new Thread(customer1);
        Thread threadCustomer2 = new Thread(customer2);
        Thread threadCustomer3 = new Thread(customer3);
        Thread threadCustomer4 = new Thread(customer4);
        Thread threadCustomer5 = new Thread(customer5);

        //this thread is waiting for poisonous drink to end all other threads.
        new Thread(() -> {
            boolean running = true;
            while (running) {
                try {
                    Drink poisonousDrink = bar.takePoisonousDrink();
                    running = false;
                    customer1.stop();
                    customer2.stop();
                    customer3.stop();
                    customer4.stop();
                    customer5.stop();
                    threadBartender.interrupt();
                    threadCustomer1.interrupt();
                    threadCustomer2.interrupt();
                    threadCustomer3.interrupt();
                    threadCustomer4.interrupt();
                    threadCustomer5.interrupt();
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }
            }
        }).start();

        threadBartender.start();
        threadCustomer1.start();
        threadCustomer2.start();
        threadCustomer3.start();
        threadCustomer4.start();
        threadCustomer5.start();


    }

}

